//
//  Cow.m
//  PixelFarm
//
//  Created by Thomas Bruno on 8/12/12.
//  Copyright (c) 2012 NaveOSS.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#import "Cow.h"
#import "cocos2d.h"

@implementation Cow


- (id)init
{
    self = [super init];
    if (self) {
		[self setupAnimations];
		
		[self runAction:[CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:walkWestAnim]]];
    }
	
    return self;
}


-(void)setupAnimations {
	CCAnimationCache *cache = [CCAnimationCache sharedAnimationCache];
	[cache addAnimationsWithFile:@"CowAnimation.plist"];

	walkWestAnim = [cache animationByName:@"WalkWest"];
	walkNorthAnim = [cache animationByName:@"WalkNorth"];
	walkSouthAnim = [cache animationByName:@"WalkSouth"];

	startEatWestAnim = [cache animationByName:@"StartEatWest"];
	startEatNorthAnim = [cache animationByName:@"StartEatNorth"];
	startEatSouthAnim = [cache animationByName:@"StartEatSouth"];
	midEatWestAnim = [cache animationByName:@"MidEatWest"];
	midEatNorthAnim = [cache animationByName:@"MidEatNorth"];
	midEatSouthAnim = [cache animationByName:@"MidEatSouth"];
	endEatWestAnim = [cache animationByName:@"EndEatWest"];
	endEatNorthAnim = [cache animationByName:@"EndEatNorth"];
	endEatSouthAnim = [cache animationByName:@"EndEatSouth"];
}

@end
