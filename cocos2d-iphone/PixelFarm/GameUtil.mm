//
// GameUtil.m
// TenFloorsDown
//
// Created by Thomas Bruno on 4/10/12.
//
// Copyright (c) 2012 Thomas Bruno
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//



#import "GameUtil.h"

@implementation GameUtil


#pragma mark - Tools Helpers

+(BOOL)isPlanterTool:(ToolTypes)tool {
	bool rtn = false;
	
	switch(tool) {
		case kToolPlantCorn:
		case kToolPlantBroccoli:
		case kToolPlantCucumber:
		case kToolPlantTomato:
		case kToolPlantPotato:
		case kToolPlantRedPepper:
		case kToolPlantCarrot:
			rtn = true;
			break;
		default:
			rtn = false;
			break;
	}
	
	return rtn;
}


+(BOOL)tileCheck:(int)tileGID toolType:(ToolTypes)tool {
	BOOL rtnVal = false;
	
	switch(tool) {
		case kToolHoe:
			switch (tileGID) {
				case kTileCenter:
				case kTileGrass1:
				case kTileGrass2:
				case kTileGrass3:
				case kTileGrass4:
				case kTileHole:
					rtnVal = true;
					break;
				default:
					rtnVal = false;
					break;
			}
			break;
		case kToolPlantCorn:
		case kToolPlantBroccoli:
		case kToolPlantCucumber:
		case kToolPlantTomato:
		case kToolPlantPotato:
		case kToolPlantRedPepper:
		case kToolPlantCarrot:
			switch (tileGID) {
				case kTileCenter:
					rtnVal = true;
					break;
				default:
					rtnVal = false;
					break;
			}
			break;
		case kToolHarvest:
			rtnVal = false;
			break;
		default:
			rtnVal = false;
			break;
	}
	
	return rtnVal;
}


#pragma mark - Tile Map Convertion Tools


// Helper used to locate the tiles around the tile at TMXPosition in each of the 8 possible directions
+(CGPoint)getTMXPositionFor:(Direction)dir fromTileAt:(CGPoint)TMXPosition withMap:(CCTMXTiledMap*)map {
	int mapWidth = map.mapSize.width;
	int mapHeight = map.mapSize.height;
	int x = TMXPosition.x;
	int y = TMXPosition.y;

	switch(dir) {
		case kDirectionNorth:
			y = y - 1;
			break;
		case kDirectionSouth:
			y = y + 1;
			break;
		case kDirectionWest:
			x = x - 1;
			break;
		case kDirectionEast:
			x = x + 1;
			break;
		case kDirectionNorthWest:
			x = x - 1;
			y = y - 1;
			break;
		case kDirectionNorthEast:
			x = x + 1;
			y = y - 1;
			break;
		case kDirectionSouthWest:
			x = x - 1;
			y = y + 1;
			break;
		case kDirectionSouthEast:
			x = x + 1;
			y = y + 1;
			break;
		case kDirectionNone:
			break;
	}

	if(x < 0 || y < 0 || x > mapWidth || y > mapHeight) {
		x = -1;
		y = -1;
	}


	return CGPointMake(x, y);
}


// Gets the TileMap Coord from a Cocos2d coordinate
+(CGPoint)getTMXPositionFromCCPosition:(CGPoint)position withMap:(CCTMXTiledMap*)map {
	CGPoint scaledPosition = ccp(position.x * CC_CONTENT_SCALE_FACTOR(),position.y * CC_CONTENT_SCALE_FACTOR());
	int x = scaledPosition.x / map.tileSize.width;
    int y = ((map.mapSize.height * map.tileSize.height) - (position.y * CC_CONTENT_SCALE_FACTOR()) ) / map.tileSize.height;

    return ccp(x, y);
}

// Gets Cocos2d coordinate for the bottom left of a tile from a tile at tmxPosition TileMap Coord
+(CGPoint)getCCPositionFromTMXPosition:(CGPoint)tmxPosition withMap:(CCTMXTiledMap*)map {
	CGSize scaledSize = CGSizeMake(map.tileSize.width / CC_CONTENT_SCALE_FACTOR(), map.tileSize.height / CC_CONTENT_SCALE_FACTOR());
	float x = tmxPosition.x * scaledSize.width;

	float y = abs((tmxPosition.y + 1) - map.mapSize.height);
	y = y * (scaledSize.height);

	return ccp(x,y);
}

+(CGPoint)getCenterOfTileCCPositionFromTMXPosition:(CGPoint)tmxPosition withMap:(CCTMXTiledMap*)map {
	CGPoint cpos = [GameUtil getCCPositionFromTMXPosition:tmxPosition withMap:map];
	int centerModx = ((map.tileSize.width/CC_CONTENT_SCALE_FACTOR()) * 0.5f);
	int centerMody = ((map.tileSize.height/CC_CONTENT_SCALE_FACTOR()) * 0.5f);

	return ccp(cpos.x + centerModx, cpos.y + centerMody);
}



#pragma mark - Play TileMap Layer Helpers


// Exhaustive case list for each state of a tile while farming. Use this
// while farming to determine what a tile should be replaced with.
+(int)resolveTile:(Direction)dir currentGID:(int)gid {
	switch(dir) {
		case kDirectionNorthWest:
			switch(gid) {
				case kTileGrass1: return kTileCornerNW;
				case kTileGrass2: return kTileCornerNW;
				case kTileGrass3: return kTileCornerNW;
				case kTileGrass4: return kTileCornerNW;
				case kTileCornerNE:	return kTileCornerNWNE;
				case kTileEdgeE: return kTileCornerNW_EdgeE;
				case kTileCornerSE: return kTileCornerNWSE;
				case kTIleEdgeS: return kTileCornerNW_EdgeS;
				case kTileCornerSW: return kTileCornerNWSW;
				case kTileCornerNESE: return kTileCornerNWNESE;
				case kTileCornerNESW: return kTileCornerNWNESW;
				case kTileCornerNE_EdgeS: return kTileCornerNWNE_EdgeS;
				case kTileCornerNESWSE: return kTileCornerNWNESWSE;
				case kTileCornerSWSE: return kTileCornerNWSWSE;

				case kTileCornerSW_EdgeE: return kTileCornerNWSW_EdgeE;
			}
			break;
		case kDirectionNorth:
			switch(gid) {
				case kTileGrass1: return kTileEdgeN;
				case kTileGrass2: return kTileEdgeN;
				case kTileGrass3: return kTileEdgeN;
				case kTileGrass4: return kTileEdgeN;
				case kTileCornerSE: return kTileCornerSE_EdgeN;
				case kTIleEdgeS: return kTileEdgeNS;
				case kTileCornerSW: return kTileCornerSW_EdgeN;
				case kTileCornerSWSE: return kTileCornerSWSE_EdgeN;
			}
			break;
		case kDirectionNorthEast:
			switch(gid) {
				case kTileGrass1: return kTileCornerNE;
				case kTileGrass2: return kTileCornerNE;
				case kTileGrass3: return kTileCornerNE;
				case kTileGrass4: return kTileCornerNE;
				case kTileCornerNW: return kTileCornerNWNE;
				case kTileCornerSE: return kTileCornerNESE;
				case kTileEdgeW: return kTileCornerNE_EdgeW;
				case kTIleEdgeS: return kTileCornerNE_EdgeS;
				case kTileCornerSW: return kTileCornerNESW;
				case kTileCornerNWSWSE: return kTileCornerNWNESWSE;
				case kTileCornerSWSE: return kTileCornerNESWSE;
				case kTileCornerNWSE: return kTileCornerNWNESE;
				case kTileCornerNW_EdgeS: return kTileCornerNWNE_EdgeS;
				case kTileCornerSE_EdgeW: return kTileCornerNESE_EdgeW;
				case kTileCornerNWSW:	return kTileCornerNWNESW;
			}
			break;
		case kDirectionWest:
			switch(gid) {
				case kTileGrass1: return kTileEdgeW;
				case kTileGrass2: return kTileEdgeW;
				case kTileGrass3: return kTileEdgeW;
				case kTileGrass4: return kTileEdgeW;
				case kTileEdgeE: return kTileEdgeWE;
				case kTileCornerSE: return kTileCornerSE_EdgeW;
				case kTileCornerNE: return kTileCornerNE_EdgeW;
				case kTileCornerNESE: return kTileCornerNESE_EdgeW;
			}
			break;
		case kDirectionEast:
			switch(gid) {
				case kTileGrass1: return kTileEdgeE;
				case kTileGrass2: return kTileEdgeE;
				case kTileGrass3: return kTileEdgeE;
				case kTileGrass4: return kTileEdgeE;
				case kTileEdgeW: return kTileEdgeWE;
				case kTileCornerNW: return kTileCornerNW_EdgeE;
				case kTileCornerSW: return kTileCornerSW_EdgeE;
				case kTileCornerNWSW: return kTileCornerNWSW_EdgeE;
			}
			break;
		case kDirectionSouthWest:
			switch(gid) {
				case kTileGrass1: return kTileCornerSW;
				case kTileGrass2: return kTileCornerSW;
				case kTileGrass3: return kTileCornerSW;
				case kTileGrass4: return kTileCornerSW;
				case kTileCornerNW: return kTileCornerNWSW;
				case kTileEdgeE: return kTileCornerSW_EdgeE;
				case kTileCornerNE: return kTileCornerNESW;
				case kTileCornerNWNESE: return kTileCornerNWNESWSE;
				case kTileCornerNESE: return kTileCornerNESWSE;
				case kTileCornerSE: return kTileCornerSWSE;
				case kTileEdgeN: return kTileCornerSW_EdgeN;
				case kTileCornerNW_EdgeE: return kTileCornerNWSW_EdgeE;
				case kTileCornerSE_EdgeN: return kTileCornerSWSE_EdgeN;
				case kTileCornerNWNE: return kTileCornerNWNESW;
				case kTileCornerNWSE: return kTileCornerNWSWSE;
			}
			break;
		case kDirectionSouth:
			switch(gid) {
				case kTileGrass1: return kTIleEdgeS;
				case kTileGrass2: return kTIleEdgeS;
				case kTileGrass3: return kTIleEdgeS;
				case kTileGrass4: return kTIleEdgeS;
				case kTileCornerNE: return kTileCornerNE_EdgeS;
				case kTileCornerNW: return kTileCornerNW_EdgeS;
				case kTileEdgeN: return kTileEdgeNS;
				case kTileCornerNWNE: return kTileCornerNWNE_EdgeS;
			}
			break;
		case kDirectionSouthEast:
			switch(gid) {
				case kTileGrass1: return kTileCornerSE;
				case kTileGrass2: return kTileCornerSE;
				case kTileGrass3: return kTileCornerSE;
				case kTileGrass4: return kTileCornerSE;
				case kTileCornerSW: return kTileCornerSWSE;
				case kTileEdgeW: return kTileCornerSE_EdgeW;
				case kTileCornerNW: return kTileCornerNWSE;
				case kTileEdgeN: return kTileCornerSE_EdgeN;
				case kTileCornerNE: return kTileCornerNESE;
				case kTileCornerNWNESW: return kTileCornerNWNESWSE;
				case kTileCornerNESW: return kTileCornerNESWSE;
				case kTileCornerNWNE: return kTileCornerNWNESE;
				case kTileCornerNE_EdgeW: return kTileCornerNESE_EdgeW;
				case kTileCornerSW_EdgeN: return kTileCornerSWSE_EdgeN;
				case kTileCornerNWSW: return kTileCornerNWSWSE;
			}
			break;
		default:
			return 0;
	}

	return 0;
}

// Exhaustive case list for each state of a tile while farming. Use this
// while farming to determine what a tile should be replaced with if trying to
// Undo changes from resolveTile.
+(int)unResolveTile:(Direction)dir currentGID:(int)gid {
	switch(dir) {
		case kDirectionNorthWest:
			switch(gid) {
				case kTileCornerNW:
					return kTileGrass1;
				case kTileCornerNWNE:
					return kTileCornerNE;
				case kTileCornerNW_EdgeE:
					return kTileEdgeE;
				case kTileCornerNWSE:
					return kTileCornerSE;
				case kTileCornerNW_EdgeS:
					return kTIleEdgeS;
				case kTileCornerNWSW:
					return kTileCornerSW;
				case kTileCornerNWNESE:
					return kTileCornerNESE;

				case kTileCornerNWNESW:
					return kTileCornerNESW;
					// This was a duplicate case. Not sure what to make of it yet
					// as its the only one that happened in the resolve to unresolve
					// conversion
					//				case kTileCornerNWNESW:
					//					return kTileCornerNWSE;

				case kTileCornerNWNE_EdgeS:
					return kTileCornerNE_EdgeS;
				case kTileCornerNWNESWSE:;
					return kTileCornerNESWSE;
				case kTileCornerNWSWSE:
					return kTileCornerSWSE;

				case kTileCornerNWSW_EdgeE:
					return kTileCornerSW_EdgeE;
			}
			break;
		case kDirectionNorth:
			switch(gid) {
				case kTileEdgeN:
					return kTileGrass1;
				case kTileCornerSE_EdgeN:
					return kTileCornerSE;
				case kTileEdgeNS:
					return kTIleEdgeS;
				case kTileCornerSW_EdgeN:
					return kTileCornerSW;
				case kTileCornerSWSE_EdgeN:
					return kTileCornerSWSE;
			}
			break;
		case kDirectionNorthEast:
			switch(gid) {
				case kTileCornerNE:
					return kTileGrass1;
				case kTileCornerNWNE:
					return kTileCornerNW;
				case kTileCornerNESE:
					return kTileCornerSE;
				case kTileCornerNE_EdgeW:
					return kTileEdgeW;
				case kTileCornerNE_EdgeS:
					return kTIleEdgeS;
				case kTileCornerNESW:
					return kTileCornerSW;
				case kTileCornerNWNESWSE:
					return kTileCornerNWSWSE;
				case kTileCornerNESWSE:
					return kTileCornerSWSE;
				case kTileCornerNWNESE:
					return kTileCornerNWSE;
				case kTileCornerNWNE_EdgeS:
					return kTileCornerNW_EdgeS;
				case kTileCornerNESE_EdgeW:
					return kTileCornerSE_EdgeW;
				case kTileCornerNWNESW:
					return kTileCornerNWSW;
			}
			break;
		case kDirectionWest:
			switch(gid) {
				case kTileEdgeW:
					return kTileGrass1;
				case kTileEdgeWE:
					return kTileEdgeE;
				case kTileCornerSE_EdgeW:
					return kTileCornerSE;
				case kTileCornerNE_EdgeW:
					return kTileCornerNE;
				case kTileCornerNESE_EdgeW:
					return kTileCornerNESE;
			}
			break;
		case kDirectionEast:
			switch(gid) {
				case kTileEdgeE:
					return kTileGrass1;
				case kTileEdgeWE:
					return kTileEdgeW;
				case kTileCornerNW_EdgeE:
					return kTileCornerNW;
				case kTileCornerSW_EdgeE:
					return kTileCornerSW;
				case kTileCornerNWSW_EdgeE:
					return kTileCornerNWSW;
			}
			break;
		case kDirectionSouthWest:
			switch(gid) {
				case kTileCornerSW:
					return kTileGrass1;
				case kTileCornerNWSW:
					return kTileCornerNW;
				case kTileCornerSW_EdgeE:
					return kTileEdgeE;
				case kTileCornerNESW:
					return kTileCornerNE;
				case kTileCornerNWNESWSE:
					return kTileCornerNWNESE;
				case kTileCornerNESWSE:
					return kTileCornerNESE;
				case kTileCornerSWSE:
					return kTileCornerSE;
				case kTileCornerSW_EdgeN:
					return kTileEdgeN;
				case kTileCornerNWSW_EdgeE:
					return kTileCornerNW_EdgeE;
				case kTileCornerSWSE_EdgeN:
					return kTileCornerSE_EdgeN;
				case kTileCornerNWNESW:
					return kTileCornerNWNE;
				case kTileCornerNWSWSE:
					return kTileCornerNWSE;
			}
			break;
		case kDirectionSouth:
			switch(gid) {
				case kTIleEdgeS:
					return kTileGrass1;
				case kTileCornerNE_EdgeS:
					return kTileCornerNE;
				case kTileCornerNW_EdgeS:
					return kTileCornerNW;
				case kTileEdgeNS:
					return kTileEdgeN;
				case kTileCornerNWNE_EdgeS:
					return kTileCornerNWNE;
			}
			break;
		case kDirectionSouthEast:
			switch(gid) {
				case kTileCornerSE:
					return kTileGrass1;
				case kTileCornerSWSE:
					return kTileCornerSW;
				case kTileCornerSE_EdgeW:
					return kTileEdgeW;
				case kTileCornerNWSE:
					return kTileCornerNW;
				case kTileCornerSE_EdgeN:
					return kTileEdgeN;
				case kTileCornerNESE:
					return kTileCornerNE;
				case kTileCornerNWNESWSE:
					return kTileCornerNWNESW;
				case kTileCornerNESWSE:
					return kTileCornerNESW;
				case kTileCornerNWNESE:
					return kTileCornerNWNE;
				case kTileCornerNESE_EdgeW:
					return kTileCornerNE_EdgeW;
				case kTileCornerSWSE_EdgeN:
					return kTileCornerSW_EdgeN;
				case kTileCornerNWSWSE:
					return kTileCornerNWSW;
			}
			break;
		default:
			return 0;
	}

	return 0;
}

// Helper to return if a tile is currently grass.
// Not the same thing as canTileBePlowed
+(BOOL)isTileGrass:(CGPoint)TMXPosition withMap:(CCTMXTiledMap*)map {
	int GID = [[map layerNamed:@"Play"] tileGIDAt:TMXPosition];
	bool isGrass = false;

	if(GID == kTileGrass1 || GID == kTileGrass2 || GID == kTileGrass3 || GID == kTileGrass4) {
		isGrass = true;
	}
	
	return isGrass;
}


// Helper to return if a tile is already plowed
+(BOOL)isTilePlowed:(CGPoint)TMXPosition withMap:(CCTMXTiledMap*)map {
	int GID = [[map layerNamed:@"Play"] tileGIDAt:TMXPosition];
	bool isPlowed = false;
	if(GID == kTileCenter) {
		isPlowed = true;
	}

	return isPlowed;
}

// Check to see if a tile can be plowed. Takes into account screen edges and tile types.
+(BOOL)canTileBePlowed:(CGPoint)TMXPosition withMap:(CCTMXTiledMap*)map {
	int GID = [[map layerNamed:@"Play"] tileGIDAt:TMXPosition];
	bool canPlow = false;

	if(GID == kTileGrass1 || GID == kTileGrass2 || GID == kTileGrass3 || GID == kTileGrass4 || GID == kTileHole) {
		int northTile = [[map layerNamed:@"Play"] tileGIDAt:[GameUtil getTMXPositionFor:kDirectionNorth fromTileAt:TMXPosition withMap:map]];
		int westTile = [[map layerNamed:@"Play"] tileGIDAt:[GameUtil getTMXPositionFor:kDirectionWest fromTileAt:TMXPosition withMap:map]];
		int southTile = [[map layerNamed:@"Play"] tileGIDAt:[GameUtil getTMXPositionFor:kDirectionSouth fromTileAt:TMXPosition withMap:map]];
		int eastTile = [[map layerNamed:@"Play"] tileGIDAt:[GameUtil getTMXPositionFor:kDirectionEast fromTileAt:TMXPosition withMap:map]];

		CCLOG(@"%d %d %d %d",northTile,westTile,southTile,eastTile);
		// TODO: Should include not being able to plow so close to rock or stump.
		if(westTile != kTileNone && northTile != kTileNone && southTile != kTileNone && eastTile != kTileNone ) {
			canPlow = true;
		}
	}
	
	return canPlow;
}

#pragma mark - Math Helpers

+(CGFloat)DegToRad:(CGFloat)degrees {
	return degrees * M_PI / 180;
}

+(CGFloat)RadToDeg:(CGFloat)radians {
	return radians * 180 / M_PI;
}

+(CGPoint)pointOnCircle:(CGPoint)center withRadius:(CGFloat)radius withAngle:(CGFloat)degrees {
	CGFloat x = center.x + radius * cos([self DegToRad:degrees]);
	CGFloat y = center.y + radius * sin([self DegToRad:degrees]);

	return CGPointMake(x, y);
}

#pragma mark - Debug Helpers

// Debug to print out the tile GID at TileMap coord TMXPosition
+(void)reportTileID:(CGPoint)TMXPosition withMap:(CCTMXTiledMap*)map {
	CCLOG(@"TileID Was: %d",[[map layerNamed:@"Values"] tileGIDAt:TMXPosition]);
}

@end
