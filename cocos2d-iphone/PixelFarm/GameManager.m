//
// GameManager.m
// PixelFarm
//
// Created by Thomas Bruno on 3/30/12.
//
// Copyright (c) 2012 Thomas Bruno
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//


#import "cocos2d.h"
#import "Constants.h"
#import "GameManager.h"
#import "SimpleAudioEngine.h"
#import "SettingsManager.h"

#import "MainMenuScene.h"
#import "TutorialScene.h"
#import "FarmScene.h"
#import "GameOverScene.h"
#import "CreditsScene.h"


@implementation GameManager

@synthesize currentTrackID;

+(id)sharedGameManager {
	static dispatch_once_t pred;
	static GameManager *gamemanager = nil;
	
	dispatch_once(&pred, ^{ gamemanager = [[self alloc] init]; });
	return gamemanager;
}


-(id)init {
	self = [super init];
	if(self != nil) {
		CCLOG(@"Game Manager Singleton, init");
		
		SettingsManager *settings = [SettingsManager sharedSettingsManager];
		musicEnabled = [[settings getString:@"MusicEnabled" defaultString:@"false"] boolValue];
		effectsEnabled = [[settings getString:@"effectsEnabled" defaultString:@"true"] boolValue];
		
		currentScene = kSceneUninitialized;
		[CDSoundEngine setMixerSampleRate:CD_SAMPLE_RATE_HIGH];
		[[CDAudioManager sharedManager] setResignBehavior:kAMRBStopPlay autoHandle:YES];

		soundEngine = [SimpleAudioEngine sharedEngine];
	}
	
	return self;
}


-(void)runSceneWithID:(GameScenes)sceneID {
	id sceneToRun = nil;
	
	switch (sceneID) {
		case kSceneMainMenu:
			sceneToRun = [MainMenuScene node];
			break;
		case kSceneTutorial:
			sceneToRun = [TutorialScene node];
			break;
		case kSceneFarm:
			sceneToRun = [FarmScene node];
			break;
        case kSceneGameOver:
            sceneToRun = [GameOverScene node];
			break;
		case kSceneCredits:
            sceneToRun = [CreditsScene node];
			break;
		default:
			CCLOG(@"Game Manager:runSceneWithID Unknown Scene!");
			return;
			break;
	}
	
	currentScene = sceneID;
	
	
	if([[CCDirector sharedDirector] runningScene] == nil) {
		[[CCDirector sharedDirector] runWithScene:sceneToRun];
		
	}
	else {
		[[CCDirector sharedDirector] replaceScene:sceneToRun];
	}
}

-(void)preloadMusicTrackWithID:(MusicTracks)trackID {
	if(!musicEnabled)
		return;
	
	switch(trackID) {
//		case kGameTrack1:
//			[soundEngine preloadBackgroundMusic:@"demo.mp3"];
//			break;
		default:
			break;
	}
}	

-(void)runMusicTrackWithID:(MusicTracks)trackID {
	if(!musicEnabled)
		return;
	
	switch(trackID) {
//		case kGameTrack1:
//			[soundEngine playBackgroundMusic:@"demo.mp3"];
//			break;
		default:
			break;
			
	}

	currentTrackID = trackID;
}

-(void)runEffectWithID:(SoundEffects)effectID {
	if(!effectsEnabled)
		return;
	
	switch(effectID) {
		default:
			break;
			
	}
}

-(void)playSoundEffectWithName:(NSString*)effectName {
	if(!effectsEnabled)
		return;

	[soundEngine setEffectsVolume:0.25f];
	[soundEngine playEffect:effectName];
}

-(void)preloadSoundEffectWithName:(NSString*)effectName {
	[soundEngine preloadBackgroundMusic:effectName];
}

@end
