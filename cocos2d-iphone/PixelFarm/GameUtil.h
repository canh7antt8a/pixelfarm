//
// GameUtil.h
// PixelFarm
//
// Created by Thomas Bruno on 4/10/12.
// Copyright (c) 2012 Thomas Bruno
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//



#import <Foundation/Foundation.h>
#import "Constants.h"
#import "cocos2d.h"


@interface GameUtil : NSObject

#pragma mark - Tools Helpers

+(BOOL)tileCheck:(int)tileGID toolType:(ToolTypes)tool;
+(BOOL)isPlanterTool:(ToolTypes)tool;

#pragma mark - Tile Map Convertion Tools

+(CGPoint)getTMXPositionFor:(Direction)dir fromTileAt:(CGPoint)TMXPosition withMap:(CCTMXTiledMap*)map;
+(CGPoint)getTMXPositionFromCCPosition:(CGPoint)position withMap:(CCTMXTiledMap*)map;
+(CGPoint)getCCPositionFromTMXPosition:(CGPoint)tmxPosition withMap:(CCTMXTiledMap*)map;
+(CGPoint)getCenterOfTileCCPositionFromTMXPosition:(CGPoint)tmxPosition withMap:(CCTMXTiledMap*)map;

#pragma mark - Play TileMap Layer Helpers

+(BOOL)canTileBePlowed:(CGPoint)TMXPosition withMap:(CCTMXTiledMap*)map;
+(BOOL)isTileGrass:(CGPoint)TMXPosition withMap:(CCTMXTiledMap*)map;
+(BOOL)isTilePlowed:(CGPoint)TMXPosition withMap:(CCTMXTiledMap*)map;

+(int)resolveTile:(Direction)dir currentGID:(int)gid;
+(int)unResolveTile:(Direction)dir currentGID:(int)gid;

#pragma mark - Math Helpers

+(CGFloat)DegToRad:(CGFloat)degrees;
+(CGFloat)RadToDeg:(CGFloat)radians;
+(CGPoint)pointOnCircle:(CGPoint)center withRadius:(CGFloat)radius withAngle:(CGFloat)degrees;

#pragma mark - Debug

+(void)reportTileID:(CGPoint)TMXPosition withMap:(CCTMXTiledMap*)map;


@end
