//
//  ToolCursor.m
//  PixelFarm
//
//  Created by Thomas Bruno on 8/10/12.
//  Copyright (c) 2012 NaveOSS. All rights reserved.
//

#import "ToolCursor.h"

@implementation ToolCursor

@synthesize isUsable;


#define SquareUsable 1000
#define SquareUnUsable 1001
#define ToolIconTag 1002

- (id)init
{
    self = [super init];
    if (self) {
		[self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"UnusableSquare.png"]];
		isUsable = false;
    }
	
    return self;
}

-(void)setCursorIndicator:(bool)usable {
	CCLOG(@"Setting Indicator to %d",usable);
	
	if(usable) {
		[self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"UsableSquare.png"]];
		isUsable = true;
	} else {
		[self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"UnusableSquare.png"]];
		isUsable = false;
	}
}

-(ToolTypes)currentTool {
	return currentTool;
}

-(void)setCurrentTool:(ToolTypes)toolType {
	switch(toolType) {
		case kToolPlantCarrot:
			toolIcon = [CCSprite spriteWithSpriteFrameName:@"produce-carrot.png"];
			[self removeChildByTag:ToolIconTag cleanup:TRUE];
			[self addChild:toolIcon z:0 tag:ToolIconTag];
			break;
		case kToolPlantCorn:
			toolIcon = [CCSprite spriteWithSpriteFrameName:@"produce-corn.png"];
			[self removeChildByTag:ToolIconTag cleanup:TRUE];
			[self addChild:toolIcon z:0 tag:ToolIconTag];
			break;
		case kToolPlantPotato:
			toolIcon = [CCSprite spriteWithSpriteFrameName:@"produce-potato.png"];
			[self removeChildByTag:ToolIconTag cleanup:TRUE];
			[self addChild:toolIcon z:0 tag:ToolIconTag];
			break;
		case kToolPlantRedPepper:
			toolIcon = [CCSprite spriteWithSpriteFrameName:@"produce-redpepper.png"];
			[self removeChildByTag:ToolIconTag cleanup:TRUE];
			[self addChild:toolIcon z:0 tag:ToolIconTag];
			break;
		case kToolPlantCucumber:
			toolIcon = [CCSprite spriteWithSpriteFrameName:@"produce-cucumber.png"];
			[self removeChildByTag:ToolIconTag cleanup:TRUE];
			[self addChild:toolIcon z:0 tag:ToolIconTag];
			break;
		case kToolPlantTomato:
			toolIcon = [CCSprite spriteWithSpriteFrameName:@"produce-tomato.png"];
			[self removeChildByTag:ToolIconTag cleanup:TRUE];
			[self addChild:toolIcon z:0 tag:ToolIconTag];
			break;
		case kToolPlantBroccoli:
			toolIcon = [CCSprite spriteWithSpriteFrameName:@"produce-broccoli.png"];
			[self removeChildByTag:ToolIconTag cleanup:TRUE];
			[self addChild:toolIcon z:0 tag:ToolIconTag];
			break;
		case kToolHoe:
			toolIcon = [CCSprite spriteWithSpriteFrameName:@"tool-hoe.png"];
			[self removeChildByTag:ToolIconTag cleanup:TRUE];
			[self addChild:toolIcon z:0 tag:ToolIconTag];
			break;
		case kToolHarvest:
			break;
		case kToolSell:
			break;
		case kToolBuy:
			break;
		default:
			[self removeChildByTag:ToolIconTag cleanup:YES];
			toolIcon = nil;
			currentTool = kToolNone;
			break;
	}

	currentTool = toolType;
	[toolIcon setPosition:ccp(16.0f,48.0f)];

}

@end
