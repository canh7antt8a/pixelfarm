//
// GameManager.h
// PixelFarm
//
// Created by Thomas Bruno on 3/30/12.
//
// Copyright (c) 2012 Thomas Bruno
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "SimpleAudioEngine.h"

@interface GameManager : NSObject {
	GameScenes currentScene;
	MusicTracks currentTrackID;
	
	SimpleAudioEngine *soundEngine;
	
	BOOL effectsEnabled;
	BOOL musicEnabled;
}

+(id)sharedGameManager;

@property (readonly,nonatomic) MusicTracks currentTrackID;

-(void)runSceneWithID:(GameScenes)sceneID;
-(void)runMusicTrackWithID:(MusicTracks)trackID;
-(void)preloadMusicTrackWithID:(MusicTracks)trackID;
-(void)runEffectWithID:(SoundEffects)effectID;

-(void)preloadSoundEffectWithName:(NSString*)effectName;
-(void)playSoundEffectWithName:(NSString*)effectName;
@end
